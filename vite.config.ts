import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import { default as path } from 'path'
import mkcert from 'vite-plugin-mkcert'
import { viteStaticCopy } from 'vite-plugin-static-copy'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        svelte(),
        mkcert(),
        viteStaticCopy({
            targets: [
                { src: 'node_modules/leaflet/dist/images/marker-icon.png', dest: '' },
                { src: 'node_modules/leaflet/dist/images/marker-shadow.png', dest: '' },
                { src: 'node_modules/leaflet/dist/images/marker-icon-2x.png', dest: '' },
            ]
          })
    ],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, './src'),
        },
    },
    publicDir: 'static',
    base: '/mapandriner',
    build: {
        target: 'es2022',
        outDir: 'public',

        rollupOptions: {
            output: {
                manualChunks(id) {
                    if (id.includes('leaflet')) return 'maps'
                    if (id.includes('node_modules')) return 'vendor'
                },
            },
        },
    },
    esbuild: {
        legalComments: 'none',
        drop: ['console', 'debugger'],
    },
    server: { https: true },
})
