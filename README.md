# Mapandriner - The Maps app for Malandriners

This simple project has been make to show to othe Malandriner how wasy is to make first steps in Svelte.

[Here](https://malandriners.gitlab.io/mapandriner/) you can play with finished app.

__Note__:  In this project I have focus only in Svelte. Important aspects as structure, security or testing, has not been have in mind.

__Note 2__: A Malandriner is a component of the comunity of [Web Reactiva](https://www.webreactiva.com/)


## How use this project?

This project has been developed to show how to follow it a step by step followind the tags:

### Step 1
In this step we will create the project with [Vite](https://vitejs.dev/) prepare the environment.

[Here are the files of this step](https://gitlab.com/malandriners/mapandriner/-/tree/Step_1)

Shown concepts:
- Create a new `Svelte` project with [Vite](https://vitejs.dev/guide/)


### Step 2
In this step we make a simple static sketch of the app with HTML5 and CSS, in App.svelte

[Here are the files of this step](https://gitlab.com/malandriners/mapandriner/-/tree/Step_2)

Shown concepts:
- Configure base URL and custom paths of `Typescript` in `jsconfig.json` file:
  - [baseUrl](https://www.typescriptlang.org/tsconfig#baseUrl)
  - [paths](https://www.typescriptlang.org/tsconfig#paths)

- Configure alias of `Vite` in `vite.config.ts` file:
  - [resolve.alias](https://vitejs.dev/config/shared-options.html#resolve-alias)

- What is a `Svelte` component, and how their minimal parts are.


### Step 3
In this step we will deconstruct the static sketch on multiple static svelte components:
- `Search`: covers searching functionality.
- `City`: shows selected city info.
- `Map`: Shows in a map the searching result and focus the selected city in the map.


[Here are the files of this step](https://gitlab.com/malandriners/mapandriner/-/tree/Step_3)

Shown concepts:
- How to combine `Svlete`components using [Slots](https://svelte.dev/docs/typescript#script-lang-ts-slots)


### Step 4
In this step we will start developing the functionalily of the `Search` component.

We start debouncing the user input after raise the search.

[Here are the files of this step](https://gitlab.com/malandriners/mapandriner/-/tree/Step_4)


Shown concepts:
- Import libraries using [`Import`](https://svelte.dev/docs/element-directives)
- Control HTML elements events using [on:](https://svelte.dev/docs/element-directives#on-eventname)  element directive.



### Step 5
In this step we will continue developing the functionalily of the `Search` component.

We continue making our component being able to:
- Show the result in a select list.
- Raise an event for letting know its container that a city has been selected.

[Here are the files of this step](https://gitlab.com/malandriners/mapandriner/-/tree/Step_5)


Shown concepts:
- Bind a variable to a property of HTML elements using [Bind](https://svelte.dev/docs/element-directives#bind-property) element directive.
- Let comunicate the component with its container via component events using [createEventDispatcher](https://svelte.dev/docs/svelte#createeventdispatcher).




### Step 6
In this step we will continue developing the functionalily of the `Search` component.

We continue making our component being able to:
- Perform the search using other service API. A Vite readable environemt variable has been added.
- Use reactivity to render in result on its list.


[Here are the files of this step](https://gitlab.com/malandriners/mapandriner/-/tree/Step_6)


Shown concepts:
- Use environment variables in `Vite` using [.env](https://vitejs.dev/guide/env-and-mode.html#env-files) file.
- Make reactive HTML render using [{#each... }](https://svelte.dev/docs/logic-blocks#each) logic block.
- Mark an statement as reactive using [$:](https://svelte.dev/docs/svelte-components#script-3-$-marks-a-statement-as-reactive) label.



### Step 7
In this step we will focus on developing the functionalily of the `Map` component.

This component will show a `Leaflet` map. 
To achive this, we will create a `component` as a library as svelte interface for this grat javascript library. 

In the other hand, we will modify the `Search` component to raise an event with the result of the search, and modify the `App` component for sending the data of the events that `Search` component raises, to the `Map`component.

At the end we will put all all components working together.


[Here are the files of this step](https://gitlab.com/malandriners/mapandriner/-/tree/Step_7)


Shown concepts:
- Pass info from container to children using [attributes and props](https://svelte.dev/docs/basic-markup#attributes-and-props).
- Custom initialize and destroy HTML elements using [Actions](https://svelte.dev/docs/svelte-action) and [use:](https://svelte.dev/docs/element-directives#use-action) directive.
- Control component using [onMount](https://svelte.dev/docs/svelte#onmount) and [onDestroy](https://svelte.dev/docs/svelte#ondestroy) lifecycle functions.
- Use context to comunication between far container and children using [setContext](https://svelte.dev/docs/svelte#setcontext) and [getContext](https://svelte.dev/docs/svelte#getcontext).



__At this point we have a basic but functional map.__
__We will play a bite more with its code to see more aspects of `Svlete`__


### Step 8
In this step we will change the way the components communicates each other.
Until this step, they passed all information using events.
Now we will change it and we will crete a `store`, to store the state of the application.

In this way, the `Search`component will change the state, and `Map` will react when the state changes.


[Here are the files of this step](https://gitlab.com/malandriners/mapandriner/-/tree/Step_9)


Shown concepts:
- Manage the application state with [writable](https://svelte.dev/docs/svelte-store#writable) and [derived](https://svelte.dev/docs/svelte-store#derived) stores.



### Step 9
In this step we will make the state changes not to be shown so rude in map and in search result list.

[Here are the files of this step](https://gitlab.com/malandriners/mapandriner/-/tree/Step_9)


Shown concepts:
- Add movement to HTML elements with [transitions](https://svelte.dev/docs/svelte-transition).


### Final step
In this step we will change the style of `App` component to give to the application a more recognocible look and feel.

[Here are the files of this step](https://gitlab.com/malandriners/mapandriner/-/tree/Final)


