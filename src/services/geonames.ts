import type { City } from '@/types'

const USERNAME: string = import.meta.env.VITE_GEONAMES_USERNAME


export const searchCityByName = async (name: string): Promise<City[]> => {
    const url = `https://secure.geonames.org/searchJSON?\
&featureCode=PPLA&featureCode=PPLA2&featureCode=PPLA3&featureCode=PPLA4\
&feature_class=P&maxRows=10&name=${name}&username=${USERNAME}`
    const result = await fetch(url)
    if(!result) return []
    const data = await result.json()
    return data.geonames.map(mapToResultItem)
}


const mapToResultItem = (item: any): City => ({
    id: item.geonameId,
    name: item.name,
    country: item.countryCode,
    admin1_name: item.adminName1,
    admin1_code: item.adminCodes1.ISO3166_2,
    population: item.population,
    lat: +item.lat,
    lng: +item.lng,
})

