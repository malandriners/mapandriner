export const debounce  = (callback: Function, milliseconds: number) => {
    let timer: NodeJS.Timer
    return function(...args: any[]) {
    const context: any = this
    clearTimeout(timer)
    timer = setTimeout(() => callback.apply(context, args), milliseconds)
  }
}
