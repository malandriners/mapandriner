import type { City } from "@/types";
import { writable, type Readable, type Writable, derived } from "svelte/store";


const createStore = () => {
    const cities: Writable<City[]> = writable([])
    const population: Readable<number> = derived(
        cities, 
        ($cities) => $cities.reduce((acum, city) => acum + city.population, 0)
    )

    const current: Writable<number> = writable(0)
    const currentCity: Readable<City | null> = derived(
        [ cities,  current ],
        ([$cities, $current]) => $cities.find(city => city.id === $current) || null
    )
    
    return { cities, current, currentCity, population }
};


export const store = createStore()
