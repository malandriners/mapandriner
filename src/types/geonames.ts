export interface City {
    id: number
    name: string
    population: number
    country: string
    admin1_name: string
    admin1_code: string
    lat: number
    lng: number
}
